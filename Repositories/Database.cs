using Microsoft.Data.SqlClient;

public abstract class Database: IDisposable
{
    protected SqlConnection conn;

    // abrir a conexão
    public Database() 
    {
        string connectionString = "Data Source=DESKTOP-PROF-LA\\MSSQLSERVER182; Initial Catalog=BDEcommerce; Integrated Security=True; TrustServerCertificate=true";

        conn = new SqlConnection(connectionString);
        conn.Open();

        Console.WriteLine("Conexão aberta");
    }

    // fechar a conexão
    public void Dispose() 
    {
        conn.Close();
        Console.WriteLine("Conexão fechada");
    }
}