CREATE DATABASE BDEcommerce
GO

USE BDEcommerce
GO

CREATE TABLE Produto
(
	ProdutoId		int				primary key		identity,
	Nome			varchar(200)	not null,
	Preco			decimal(9,2)	not null
)

INSERT INTO Produto VALUES('L�pis', 1.50)
INSERT INTO Produto VALUES('Caneta', 2.50)
INSERT INTO Produto VALUES('Caderno', 7.20)

SELECT * FROM Produto